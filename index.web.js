import React from 'react';
import ReactDOM from 'react-dom';
import App from './src/web/app';

ReactDOM.render(<App />, document.getElementById('app'));
