import React from 'react';
import {
    Button,
    Alert
} from 'react-native';

const HomeComponent = ({ handleOnClick }) => {
    return <Button
        title="Press me"
        onPress={() => Alert.alert('Simple Button pressed')}
    />

};

export default HomeComponent;