import React from 'react';
import HomeComponent from '../component/HomeComponent'

const Home = () => {
    const handleOnclick = (value) => {
        console.log('container')
        alert(value)
    }

    return <HomeComponent handleOnclick={(value) => handleOnclick(value)} />
}

export default Home;